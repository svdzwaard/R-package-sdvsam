detach("package:graphics", unload=TRUE)
detach("package:grDevices", unload=TRUE)
detach("package:httr", unload=TRUE)
detach("package:jsonlite", unload=TRUE)
detach("package:methods", unload=TRUE)
detach("package:purrr", unload=TRUE)
detach("package:RCurl", unload=TRUE)
detach("package:readr", unload=TRUE)
detach("package:stringr", unload=TRUE)
detach("package:stats", unload=TRUE)
detach("package:tidyr", unload=TRUE)
detach("package:tibble", unload=TRUE)
detach("package:tidyverse", unload=TRUE)
detach("package:utils", unload=TRUE)
detach("package:ggplot2", unload=TRUE)
detach("package:forcats", unload=TRUE)
detach("package:dplyr", unload=TRUE)
detach("package:datasets", unload=TRUE)
detach("package:bitops", unload=TRUE)
search()
#' Retrieve token for retrieval of Mylaps segment data in speed skating
#'
#' Using your credential information, you can acquire an API token to retrieve Mylaps segment data from the database of the Dutch Royal Speed Skating Federation (KNSB) in \code{\link{Thialf}}
#'
#' @param client_id client identifier used to obtain API token
#' @param client_secret client secret used to obtain API token
#'
#' @return API token to access the KNSB Mylaps database in Thialf
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' my_token <- get_knsb_token(client_id = "myClientID",
#'                            client_secret = "myClientSecret")
#'
get_knsb_token <- function(client_id, client_secret) {
# Packages
require(httr)
require(jsonlite)
require(RCurl)
require(tidyverse)
# Check presence of input arguments
if (is.null(client_id)) {
warning("No client id present")
}
if (is.null(client_secret)) {
warning("No client secret present")
}
# Set database related to credentials
scope         <- 'KNSBx2ConnectApi'
# Set-up secret, authorization headers and body
id_secret     <- base64(paste(client_id,client_secret,sep=':'))[[1]]
my_headers    <- add_headers(c(Authorization=paste('Basic', id_secret,sep=' ')))
my_body       <- list(grant_type='client_credentials', scope = scope)
# Retrieve authorization token for database connection
my_token      <- content(POST("https://identityx2connect.azurewebsites.net/connect/token",
my_headers,
body = my_body,
encode = 'form'))
# Retrieve token
return(my_token)
}
#' Retrieve token for retrieval of Mylaps segment data in speed skating
#'
#' Using your credential information, you can acquire an API token to retrieve Mylaps segment data from the database of the Dutch Royal Speed Skating Federation (KNSB) in \code{\link{Thialf}}
#'
#' @param client_id client identifier used to obtain API token
#' @param client_secret client secret used to obtain API token
#'
#' @return API token to access the KNSB Mylaps database in Thialf
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' my_token <- get_knsb_token(client_id = "myClientID",
#'                            client_secret = "myClientSecret")
#'
get_knsb_token <- function(client_id, client_secret) {
# Packages
require(httr)
require(jsonlite)
require(RCurl)
require(tidyverse)
# Check presence of input arguments
if (is.null(client_id)) {
warning("No client id present")
}
if (is.null(client_secret)) {
warning("No client secret present")
}
# Set database related to credentials
scope         <- 'KNSBx2ConnectApi'
# Set-up secret, authorization headers and body
id_secret     <- base64(paste(client_id,client_secret,sep=':'))[[1]]
my_headers    <- add_headers(c(Authorization=paste('Basic', id_secret,sep=' ')))
my_body       <- list(grant_type='client_credentials', scope = scope)
# Retrieve authorization token for database connection
my_token      <- content(POST("https://identityx2connect.azurewebsites.net/connect/token",
my_headers,
body = my_body,
encode = 'form'))
# Retrieve token
return(my_token)
}
?object.size
??object.size
??object
#' Retrieve token for retrieval of Mylaps segment data in speed skating
#'
#' Using your credential information, you can acquire an API token to retrieve Mylaps segment data from the database of the Dutch Royal Speed Skating Federation (KNSB) in \code{\link{Thialf}}
#'
#' @param client_id client identifier used to obtain API token
#' @param client_secret client secret used to obtain API token
#'
#' @return API token to access the KNSB Mylaps database in Thialf
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' my_token <- get_knsb_token(client_id = "myClientID",
#'                            client_secret = "myClientSecret")
#'
get_knsb_token <- function(client_id, client_secret) {
# Packages
require(httr)
require(jsonlite)
require(RCurl)
require(tidyverse)
# Check presence of input arguments
if (is.null(client_id)) {
warning("No client id present")
}
if (is.null(client_secret)) {
warning("No client secret present")
}
# Set database related to credentials
scope         <- 'KNSBx2ConnectApi'
# Set-up secret, authorization headers and body
id_secret     <- base64(paste(client_id,client_secret,sep=':'))[[1]]
my_headers    <- add_headers(c(Authorization=paste('Basic', id_secret,sep=' ')))
my_body       <- list(grant_type='client_credentials', scope = scope)
# Retrieve authorization token for database connection
my_token      <- content(POST("https://identityx2connect.azurewebsites.net/connect/token",
my_headers,
body = my_body,
encode = 'form'))
# Retrieve token
return(my_token)
}
#' Retrieve Mylaps segment data of passings in speed skating
#'
#' Using your API token retrieve the Mylaps segment data of passings for a specific speed skater
#' based on his/her transponder label or number from the database of the Dutch Royal Speed Skating Federation (KNSB) in \code{\link{Thialf}}
#'
#' @param token API token obtained by the function get_knsb_token
#' @param start_time start time of interval to obtain mylaps data (in ISO datetime format: 2020-01-01 08:08:08)
#' @param end_time end time of interval to obtain mylaps data (in ISO datetime format: 2020-01-01 08:08:08)
#' @param tansponder Mylaps transponder code (in label, NOT number) to retrieve segment data from
#'
#' @return data frame with mylaps segment data
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' my_data <- retrieve_mylaps_data(token       =  my_token,
#'                                 start_time  = "2020-01-01 08:08:08",
#'                                 end_time    = "2020-01-01 10:10:10",
#'                                 transponder = "myTransponderLabel")
retrieve_mylaps_passings <- function(token, start_time, end_time, transponder=NULL) {
# Call: events     - AuxEvents
#       passings   - Passings
#       laptimes   - Laptimes
#
# Packages
require(httr)
require(jsonlite)
require(RCurl)
require(tidyverse)
# Set authorization based on bearer access token.
auth_header   <- add_headers('Authorization'= paste('Bearer ',my_token$access_token))
# Specific start and end time in correct format
start_time    <- strftime(as.POSIXlt(start_time, "CET"), "%Y-%m-%dT%H:%M:%SZ")
end_time      <- strftime(as.POSIXlt(end_time, "CET"), "%Y-%m-%dT%H:%M:%SZ")
# Check if transponder is empty, numeric or specified as a label
numbers_only <- function(x) !grepl("\\D", x)
if (is.null(transponder)) {
transponder_call <- ""
} else if (numbers_only(transponder)) {
transponder_call <- paste0("&TransponderId=",transponder)
} else {
transponder_call <- paste0("&TransponderLabel=",transponder)
}
# Get Mylaps segment data
response      <- content(GET(paste0('https://apix2connect.azurewebsites.net/api/Passings?StartDate=',start_time,'&EndDate=',end_time,transponder_call),
auth_header))
response      <- bind_rows(response)
# Return response with data
return(response)
}
library("utils", lib.loc="/Library/Frameworks/R.framework/Versions/3.5/Resources/library")
#' Retrieve Mylaps segment data of passings in speed skating
#'
#' Using your API token retrieve the Mylaps segment data of passings for a specific speed skater
#' based on his/her transponder label or number from the database of the Dutch Royal Speed Skating Federation (KNSB) in \code{\link{Thialf}}
#'
#' @param token API token obtained by the function get_knsb_token
#' @param start_time start time of interval to obtain mylaps data (in ISO datetime format: 2020-01-01 08:08:08)
#' @param end_time end time of interval to obtain mylaps data (in ISO datetime format: 2020-01-01 08:08:08)
#' @param tansponder Mylaps transponder code (in label, NOT number) to retrieve segment data from
#'
#' @return data frame with mylaps segment data
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' my_data <- retrieve_mylaps_data(token       =  my_token,
#'                                 start_time  = "2020-01-01 08:08:08",
#'                                 end_time    = "2020-01-01 10:10:10",
#'                                 transponder = "myTransponderLabel")
retrieve_mylaps_passings <- function(token, start_time, end_time, transponder=NULL) {
# Call: events     - AuxEvents
#       passings   - Passings
#       laptimes   - Laptimes
#
# Packages
require(httr)
require(jsonlite)
require(RCurl)
require(tidyverse)
# Set authorization based on bearer access token.
auth_header   <- add_headers('Authorization'= paste('Bearer ',my_token$access_token))
# Specific start and end time in correct format
start_time    <- strftime(as.POSIXlt(start_time, "CET"), "%Y-%m-%dT%H:%M:%SZ")
end_time      <- strftime(as.POSIXlt(end_time, "CET"), "%Y-%m-%dT%H:%M:%SZ")
# Check if transponder is empty, numeric or specified as a label
numbers_only <- function(x) !grepl("\\D", x)
if (is.null(transponder)) {
transponder_call <- ""
} else if (numbers_only(transponder)) {
transponder_call <- paste0("&TransponderId=",transponder)
} else {
transponder_call <- paste0("&TransponderLabel=",transponder)
}
# Get Mylaps segment data
response      <- content(GET(paste0('https://apix2connect.azurewebsites.net/api/Passings?StartDate=',start_time,'&EndDate=',end_time,transponder_call),
auth_header))
response      <- bind_rows(response)
# Return response with data
return(response)
}
#' Retrieve token for retrieval of Mylaps segment data in speed skating
#'
#' Using your credential information, you can acquire an API token to retrieve Mylaps segment data from the database of the Dutch Royal Speed Skating Federation (KNSB) in \code{\link{Thialf}}
#'
#' @param client_id client identifier used to obtain API token
#' @param client_secret client secret used to obtain API token
#'
#' @return API token to access the KNSB Mylaps database in Thialf
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' my_token <- get_knsb_token(client_id = "myClientID",
#'                            client_secret = "myClientSecret")
#'
get_knsb_token <- function(client_id, client_secret) {
# Packages
require(httr)
require(jsonlite)
require(RCurl)
require(tidyverse)
# Check presence of input arguments
if (is.null(client_id)) {
warning("No client id present")
}
if (is.null(client_secret)) {
warning("No client secret present")
}
# Set database related to credentials
scope         <- 'KNSBx2ConnectApi'
# Set-up secret, authorization headers and body
id_secret     <- base64(paste(client_id,client_secret,sep=':'))[[1]]
my_headers    <- add_headers(c(Authorization=paste('Basic', id_secret,sep=' ')))
my_body       <- list(grant_type='client_credentials', scope = scope)
# Retrieve authorization token for database connection
my_token      <- content(POST("https://identityx2connect.azurewebsites.net/connect/token",
my_headers,
body = my_body,
encode = 'form'))
# Retrieve token
return(my_token)
}
View(get_knsb_token)
View(get_knsb_token)
#' Retrieve token for retrieval of Mylaps segment data in speed skating
#'
#' Using your credential information, you can acquire an API token to retrieve Mylaps segment data from the database of the Dutch Royal Speed Skating Federation (KNSB) in \code{\link{Thialf}}
#'
#' @param client_id client identifier used to obtain API token
#' @param client_secret client secret used to obtain API token
#'
#' @return API token to access the KNSB Mylaps database in Thialf
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' my_token <- get_knsb_token(client_id = "myClientID",
#'                            client_secret = "myClientSecret")
#'
get_knsb_token <- function(client_id, client_secret) {
# Packages
require(httr)
require(jsonlite)
require(RCurl)
require(tidyverse)
# Check presence of input arguments
if (is.null(client_id)) {
warning("No client id present")
}
if (is.null(client_secret)) {
warning("No client secret present")
}
# Set database related to credentials
scope         <- 'KNSBx2ConnectApi'
# Set-up secret, authorization headers and body
id_secret     <- base64(paste(client_id,client_secret,sep=':'))[[1]]
my_headers    <- add_headers(c(Authorization=paste('Basic', id_secret,sep=' ')))
my_body       <- list(grant_type='client_credentials', scope = scope)
# Retrieve authorization token for database connection
my_token      <- content(POST("https://identityx2connect.azurewebsites.net/connect/token",
my_headers,
body = my_body,
encode = 'form'))
# Retrieve token
return(my_token)
}
library(utils)
#' Retrieve token for retrieval of Mylaps segment data in speed skating
#'
#' Using your credential information, you can acquire an API token to retrieve Mylaps segment data from the database of the Dutch Royal Speed Skating Federation (KNSB) in \code{\link{Thialf}}
#'
#' @param client_id client identifier used to obtain API token
#' @param client_secret client secret used to obtain API token
#'
#' @return API token to access the KNSB Mylaps database in Thialf
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' my_token <- get_knsb_token(client_id = "myClientID",
#'                            client_secret = "myClientSecret")
#'
get_knsb_token <- function(client_id, client_secret) {
# Packages
require(httr)
require(jsonlite)
require(RCurl)
require(tidyverse)
# Check presence of input arguments
if (is.null(client_id)) {
warning("No client id present")
}
if (is.null(client_secret)) {
warning("No client secret present")
}
# Set database related to credentials
scope         <- 'KNSBx2ConnectApi'
# Set-up secret, authorization headers and body
id_secret     <- base64(paste(client_id,client_secret,sep=':'))[[1]]
my_headers    <- add_headers(c(Authorization=paste('Basic', id_secret,sep=' ')))
my_body       <- list(grant_type='client_credentials', scope = scope)
# Retrieve authorization token for database connection
my_token      <- content(POST("https://identityx2connect.azurewebsites.net/connect/token",
my_headers,
body = my_body,
encode = 'form'))
# Retrieve token
return(my_token)
}
#' Retrieve token for retrieval of Mylaps segment data in speed skating
#'
#' Using your credential information, you can acquire an API token to retrieve Mylaps segment data from the database of the Dutch Royal Speed Skating Federation (KNSB) in \code{\link{Thialf}}
#'
#' @param client_id client identifier used to obtain API token
#' @param client_secret client secret used to obtain API token
#'
#' @return API token to access the KNSB Mylaps database in Thialf
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' my_token <- get_knsb_token(client_id = "myClientID",
#'                            client_secret = "myClientSecret")
#'
get_knsb_token <- function(client_id, client_secret) {
# Packages
require(httr)
require(jsonlite)
require(RCurl)
require(tidyverse)
# Check presence of input arguments
if (is.null(client_id)) {
warning("No client id present")
}
if (is.null(client_secret)) {
warning("No client secret present")
}
# Set database related to credentials
scope         <- 'KNSBx2ConnectApi'
# Set-up secret, authorization headers and body
id_secret     <- base64(paste(client_id,client_secret,sep=':'))[[1]]
my_headers    <- add_headers(c(Authorization=paste('Basic', id_secret,sep=' ')))
my_body       <- list(grant_type='client_credentials', scope = scope)
# Retrieve authorization token for database connection
my_token      <- content(POST("https://identityx2connect.azurewebsites.net/connect/token",
my_headers,
body = my_body,
encode = 'form'))
# Retrieve token
return(my_token)
}
#' Retrieve Mylaps segment data of passings in speed skating
#'
#' Using your API token retrieve the Mylaps segment data of passings for a specific speed skater
#' based on his/her transponder label or number from the database of the Dutch Royal Speed Skating Federation (KNSB) in \code{\link{Thialf}}
#'
#' @param token API token obtained by the function get_knsb_token
#' @param start_time start time of interval to obtain mylaps data (in ISO datetime format: 2020-01-01 08:08:08)
#' @param end_time end time of interval to obtain mylaps data (in ISO datetime format: 2020-01-01 08:08:08)
#' @param tansponder Mylaps transponder code (in label, NOT number) to retrieve segment data from
#'
#' @return data frame with mylaps segment data
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' my_data <- retrieve_mylaps_data(token       =  my_token,
#'                                 start_time  = "2020-01-01 08:08:08",
#'                                 end_time    = "2020-01-01 10:10:10",
#'                                 transponder = "myTransponderLabel")
retrieve_mylaps_passings <- function(token, start_time, end_time, transponder=NULL) {
# Call: events     - AuxEvents
#       passings   - Passings
#       laptimes   - Laptimes
#
# Packages
require(httr)
require(jsonlite)
require(RCurl)
require(tidyverse)
# Set authorization based on bearer access token.
auth_header   <- add_headers('Authorization'= paste('Bearer ',my_token$access_token))
# Specific start and end time in correct format
start_time    <- strftime(as.POSIXlt(start_time, "CET"), "%Y-%m-%dT%H:%M:%SZ")
end_time      <- strftime(as.POSIXlt(end_time, "CET"), "%Y-%m-%dT%H:%M:%SZ")
# Check if transponder is empty, numeric or specified as a label
numbers_only <- function(x) !grepl("\\D", x)
if (is.null(transponder)) {
transponder_call <- ""
} else if (numbers_only(transponder)) {
transponder_call <- paste0("&TransponderId=",transponder)
} else {
transponder_call <- paste0("&TransponderLabel=",transponder)
}
# Get Mylaps segment data
response      <- content(GET(paste0('https://apix2connect.azurewebsites.net/api/Passings?StartDate=',start_time,'&EndDate=',end_time,transponder_call),
auth_header))
response      <- bind_rows(response)
# Return response with data
return(response)
}
devtools::document()
devtools::document()
devtools::document()
devtools::document()
setwd(..)
setwd("..")
getwd()
install.packages("SDVmylaps")
install.packages("SDVmylaps")
install.packages("SDVmylaps")
install.packages("SDVmylaps")
?bind_rows
??bind_rows
devtools::document()
devtools::document()
document()
devtools::document()
?root
??root
library(root)
library(SDVmylaps)
?get_knsb_token
?retrieve_mylaps_passings
library(SDVmylaps)
document9)
devtools:document()
devtools::document()
library(SDVmylaps)
devtools::document()
devtools::document()
devtools::document()
