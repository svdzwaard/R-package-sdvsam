---
output:
  html_document: default
  pdf_document: default
---

# Health data dictionary: general
<hr>

<h3> Content </h3>


This dataset includes daily data on the mortality rate, population size and weather conditions in the Netherlands from 1995 until 2017.
Weather conditions data was provided by the Royal Netherlands Meteorological Institute (KNMI) using the weather stations in The Netherlands. 

Below you can find the definitions of each variable in the dataset.

<hr>

| Variable | Description |
|:---------------------------------------|:----------------------------------------------------------------------------|
| | |
| **Date**                           | |
| `  date`                           | date on which mortality, population and weather data was collected |                        
| `  year                          ` | year | 
| `  month                         ` | month |
| `  day                           ` | day of the month [1-31] |
| `  weekday                       ` | day of the week  [1-7] (starting on Monday) |
| `  yearday                       ` | day of the year  [1-366] |
| | |
| **Mortality**                      | |
| `  deaths_daily_total            ` | total number of deaths per day |
| `  deaths_daily_total_male       ` | number of deaths per day for males |
| `  deaths_daily_total_female     ` | number of deaths per day for females |
| `  deaths_daily_male_under_65    ` | number of deaths per day for males younger than 65 years old |
| `  deaths_daily_male_65_to_80    ` | number of deaths per day for males between 65 and 80 years old |
| `  deaths_daily_male_above_80    ` | number of deaths per day for males of 80 years and older |
| `  deaths_daily_female_under_65  ` | number of deaths per day for females younger than 65 years old |
| `  deaths_daily_female_65_to_80  ` | number of deaths per day for females between 65 and 80 years old |
| `  deaths_daily_female_above_80  ` | number of deaths per day for females of 80 years and older |
| | |
| **Population**                     | |
| `  population_total_male         ` | total population of males |
| `  population_total_female       ` | total population of females |
| `  population_total_under_65     ` | total population of people younger than 65 years old |
| `  population_total_65_to_80     ` | total population of people  between 65 and 80 years old |
| `  population_total_above_80     ` | total population of people of 80 years and older |
| `  population_male_under_65      ` | population of males younger than 65 years old |
| `  population_male_65_to_80      ` | population of males between 65 and 80 years old |
| `  population_male_above_80      ` | population of males of 80 years and older |
| `  population_female_under_65    ` | population of females younger than 65 years old |
| `  population_female_65_to_80    ` | population of females between 65 and 80 years old |
| `  population_female_above_80    ` | population of females of 80 years and older |
| | |
| **Weather conditions**             | |
| `  weather_knmi_wind_FHVEC       ` | vector mean windspeed (in 0.1 m/s) from the KNMI |
| `  weather_knmi_wind_FG          ` | daily mean windspeed (in 0.1 m/s) from the KNMI |
| `  weather_knmi_wind_FHX         ` | maximum hourly mean windspeed (in 0.1 m/s) from the KNMI |
| `  weather_knmi_wind_FHN         ` | minimum hourly mean windspeed (in 0.1 m/s) from the KNMI|
| `  weather_knmi_temp_TG          ` | daily mean temperature in (0.1 degrees Celsius) from the KNMI |
| `  weather_knmi_temp_TX          ` | maximum hourly temperature (in 0.1 degrees Celsius) from the KNMI |
| `  weather_knmi_temp_TN          ` | minimum hourly temperature (in 0.1 degrees Celsius) from the KNMI |
| `  weather_knmi_humidity_UG      ` | daily mean relative atmospheric humidity (in percents) from the KNMI |
| `  weather_knmi_humidity_UX      ` | maximum relative atmospheric humidity (in percents) from the KNMI |
| `  weather_knmi_humidity_UN      ` | minimum relative atmospheric humidity (in percents) from the KNMI |
| | |