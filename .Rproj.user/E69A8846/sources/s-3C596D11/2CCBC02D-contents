#' Retrieve token for retrieval of Mylaps segment data in speed skating
#'
#' Using your credential information, you can acquire an API token to retrieve Mylaps segment data from the database of the Dutch Royal Speed Skating Federation (KNSB) in Thialf
#'
#' @param client_id client identifier used to obtain API token
#' @param client_secret client secret used to obtain API token
#'
#' @return API token to access the KNSB Mylaps database in Thialf
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords API token, Database, Segment Analysis Mylaps
#'
#' @examples
#' # get_knsb_token(client_id = "myClientID",
#' #                client_secret = "myClientSecret")
#'
#' @import httr
#' @import jsonlite
#' @import RCurl

get_knsb_token <- function(client_id, client_secret) {


    # Check presence of input arguments
    if (is.null(client_id)) {
      warning("No client id present")
    }
    if (is.null(client_secret)) {
      warning("No client secret present")
    }


    # Set database related to credentials
    scope         <- 'KNSBx2ConnectApi'


    # Set-up secret, authorization headers and body
    id_secret     <- base64(paste(client_id,client_secret,sep=':'))[[1]]
    my_headers    <- add_headers(c(Authorization=paste('Basic', id_secret,sep=' ')))
    my_body       <- list(grant_type='client_credentials', scope = scope)


    # Retrieve authorization token for database connection
    my_token      <- content(POST("https://identityx2connect.azurewebsites.net/connect/token",
                                  my_headers,
                                  body = my_body,
                                  encode = 'form'))

    # Retrieve token
    return(my_token)
}
