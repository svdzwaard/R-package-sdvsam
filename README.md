<!-- rmarkdown v1 -->
<!-- README.md is generated from README.Rmd. Please edit that file -->



[![build](https://img.shields.io/badge/build-passing-succes.svg)](https://img.shields.io/badge/build-passing-succes.svg)
[![license](https://img.shields.io/badge/licence-GPL--3-blue.svg)](https://www.gnu.org/licenses/gpl.html)
[![version](https://img.shields.io/badge/Package%20version-0.1.0-orange.svg)](https://img.shields.io/badge/Package%20version-0.1.0-orange.svg)

# sdvsam <img src="man/figures/skate.png" align="right" alt="" width="160" />

sdvsam is designed to make it quick and easy to retrieve Mylaps segment data from the database of the Dutch Royal Speed Skating Federation. The data is collected from transponders that the speed skaters wear during their skating sessions on the ice rink Thialf in Heerenveen, The Netherlands.
Each time the transponder passess one of the 12 loops in the ice, a record is collected. Using the proper credentials, you can obtain these data records based on the transponder labels and a time interval. You can see SDVsam in action at <https://pkgdown.r-lib.org>. Learn more in `?get_knsb_token` or `?retrieve_mylaps_passings`.

## Installation


```r
# Install development version from Gitlab
devtools::install_gitlab("svdzwaard/R-package-sdvsam")

```

## Retrieve access to the database

Using your credentials, you can obtain a bearer API token to be able to request data from a secured database of the Dutch Royal Speed Skating Federation. Note that the token expires after 1 hour.


```r
# Retrieve API-token using the proper credentials
sdvsam::get_knsb_token(client_id     = "organization_name", 
                       client_secret = "XXXXXXX")

```

This will generate a `API token` that can be used to access the database. Data can be retrieved using your own requests based on the API endpoints 
[as described in the documentation](https://apix2connect.azurewebsites.net/swagger/index.html). You can also use the `retrieve_segment_data` function if you want to directly obtain the data more easily.

## Obtain passings

Using the `retrieve_segment_data` function you can request the data of a specific transponder of a speed skater within a specified time interval. 


```r
# Retrieve Mylaps passings data using your token
sdvsam::retrieve_segment_data(token       =  my_token,
                              start_time  = "2020-01-01 08:08:08",
                              end_time    = "2020-01-01 10:10:10",
                              transponder = "XXX-XXXXX")

```

Note that the transponder should be specified as the label. Also, start and end time should be specified in ISO datetime formatting.
You will retrieve a dataframe including all the passings within the time interval, with a maximum of 10.000 records (which is the rate limit).

## Powered by Sport Data Valley© 
Please note that this project is released by Sport Data Valley [click here for more information](https://www.sportinnovator.nl/sport-data-valley/).
<br>
<p align="center">
<img src="man/figures/SDV_logo.png" alt="" width="150" />
</p>
